<?php

// Custom event

require_once('./vendor/autoload.php');
require_once('config.php');

date_default_timezone_set('UTC');

$chat_info = $_POST['message_ok'];

$channel_name = null;

if( !isset($_POST['message_ok']) ){
    header("HTTP/1.0 400 Bad Request");
    echo('chat_info must be provided');
}

$channel_name = 'presence-dev-chat';

$pusher = new Pusher(APP_KEY, APP_SECRET, APP_ID);
$data = array("id" => '123');
$response = $pusher->trigger($channel_name, 'message-status', $data, null, true);

header('Cache-Control: no-cache, must-revalidate');
header('Content-type: application/json');

$result = array('pusherResponse' => $response);
echo(json_encode($result));
